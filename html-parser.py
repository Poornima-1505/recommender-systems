import requests
import urllib.request
import time
from bs4 import BeautifulSoup


url = 'https://www.amazon.com/UGG-Womens-Scuffette-II-Slipper/dp/B086C1MTH9?pd_rd_w=MS7y6&pf_rd_p=b03f717b-3363-4d4a-8406-b674ead295ee&pf_rd_r=7D3SPQY0P1SPHBM1TM9X&pd_rd_r=4aaf0759-ca25-4599-a278-4fe87064514f&pd_rd_wg=UjZ93'
response = requests.get(url)

html_soup = BeautifulSoup(response.text, 'html.parser')

# names = html_soup.find_all('div', class_ = "product-card__link-overlay")
names = html_soup.find_all("a",class_ = "product-card__link-overlay")
prices = html_soup.find_all("div",class_ = "product-price css-11s12ax is--current-price")
colour = html_soup.find_all("div",class_ = "product-card__product-count")

product_names = []
product_prices = []
colours = []

for rows in names:
    product_names.append(rows.get_text())

for rows in prices:
    product_prices.append(rows.get_text().strip().strip('$'))     

for rows in colour:
    colours.append(rows.get_text())   


print(len(product_names))
print(len(product_prices))
print(len(colours))

print((product_names[0]))
print((product_prices[0]))
print((colours[0]))